import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HompePage extends StatelessWidget {
  const HompePage({super.key});

  void logOut() async{
    await FirebaseAuth.instance.signOut();
 }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            Text("Selamat Datang di Home Page!"),
            ElevatedButton.icon(
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.purple)),
              onPressed: logOut,
              icon: Icon(Icons.logout_outlined),
              label: Text("Logout"),
              ),
          ],
        ),
      ),
    );
  }
}